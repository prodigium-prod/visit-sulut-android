import React, { useState, useEffect } from 'react';
import { SafeAreaView, StyleSheet } from 'react-native';
import Swiper from 'react-native-deck-swiper';
import { TinderCard } from '../../components/TinderCard';
import { getTinderTour } from '../../data/tindertour';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

const TinderTour = () => {
  const [cardsData, setCardsData] = useState([]);
  userId = useSelector(state => state.auth.login.user.id);

  useEffect(() => {
    fetchTinderTourData();
  }, []);

  const fetchTinderTourData = async () => {
    try {
      const data = await getTinderTour();
      // Assuming the ID field in your Tinder tour data is named "id"
      const cardsWithDataAndId = data.map((cardData) => ({
        ...cardData,
        id: cardData.id, // Replace "id" with the actual name of the ID field in your data
      }));
      setCardsData(cardsWithDataAndId);
    } catch (error) {
      console.error('Error:', error);
    }
  };
  

  const onSwipeLeft = async (index) => {
    const swipedCardId = cardsData[index].id;
    console.log('Swiped left on card with ID:', swipedCardId);
    console.log('User ID:', userId);
  
    try {
      const response = await fetch('http://b.visit-northsulawesi.com/api/swipe/create', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          swipedCardId,
          userId,
          isLiked: false, // Set this value based on left swipe or right swipe logic
        }),
      });
  
      const data = await response.json();
      console.log('API Response:', data);
  
      // You can add your logic here for what happens when the API call is successful
    } catch (error) {
      console.error('Error:', error);
      // You can add your logic here for error handling
    }
  };
  
  const onSwipeRight = async (index) => {
    const swipedCardId = cardsData[index].id;
    console.log('Swiped right on card with ID:', swipedCardId);
    console.log('User ID:', userId);
  
    try {
      const response = await fetch('http://b.visit-northsulawesi.com/api/swipe/create', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          swipedCardId,
          userId,
          isLiked: true, // Set this value based on left swipe or right swipe logic
        }),
      });
  
      const data = await response.json();
      console.log('API Response:', data);
  
      // You can add your logic here for what happens when the API call is successful
    } catch (error) {
      console.error('Error:', error);
      // You can add your logic here for error handling
    }
  };
  
  

  console.log("Data Tinder : ",cardsData);

  const renderCard = (cardData, index) => <TinderCard key={index} {...cardData} />;

  return (
    <SafeAreaView style={styles.container}>
      <Swiper
        cards={cardsData}
        renderCard={renderCard}
        infinite
        backgroundColor="white"
        cardHorizontalMargin={0}
        stackSize={2}
        onSwipedLeft={onSwipeLeft}
        onSwipedRight={onSwipeRight}
      />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
});

export default TinderTour;
