// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyATvunKH9s4O7nGwdmAGos3PfoOKdL0LSU",
  authDomain: "visitsulut-4367e.firebaseapp.com",
  projectId: "visitsulut-4367e",
  storageBucket: "visitsulut-4367e.appspot.com",
  messagingSenderId: "632688105777",
  appId: "1:632688105777:web:b54f72c81b54be6779b810",
  measurementId: "G-G86Q4N6EY9"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);